import logging

from flask import jsonify, Flask
from flasgger import Swagger


# App code imports
# -------------------------------------------------------------------------------------
from fake_service.version_0_1_0.main import app_v0_1_0, version_url_name
from fake_service.config import Config
import fake_service.version_0_1_0.routes


# Global configuration
# -------------------------------------------------------------------------------------
config = Config()


# Framework globals
# -------------------------------------------------------------------------------------
flask_app = None
current_app = app_v0_1_0

# App
flask_app = Flask(__name__)
flask_app.secret_key = 'TTS96tKYthZh2V2jO7Bwi1c4BO0BFYfe8YnDegkg'
flask_app.register_blueprint(current_app, url_prefix='/{}'.format(version_url_name))
flask_app.config.update(api_version_name=current_app.name)

# Flask plugins
swagger = Swagger(flask_app)


# App code
# -------------------------------------------------------------------------------------
@flask_app.route('/')
def root() -> str:
    return 'The website is up and running!'


@flask_app.route('/version/')
def version() -> str:
    """
    Return the api version
    ---
    tags:
      - app
    responses:
      200:
        description: The API version
        schema:
          id: version
          properties:
            version:
              type: string
              default: 0_1_0
    """
    return jsonify({'version': config.app_version})


@flask_app.route('/names/')
def names() -> str:
    """
    Return the app name
    ---
    tags:
      - app
    responses:
      200:
        description: The app name
        schema:
          id: version
          properties:
            version:
              type: string
              default: 0_1_0
    """
    return jsonify({'app_name': config.app_name, 'app_short_name': config.app_short_name})


@flask_app.route('/routes/')
def routes() -> str:
    """
    Return all of the routes
    ---
    tags:
      - app
    responses:
      200:
        description: The service routes
        schema:
          id: version
          properties:
            version:
              type: string
              default: 0_1_0
    """
    rules = [x for x in flask_app.url_map.iter_rules()]
    routes = {x.rule: '{} {}'.format(x.endpoint, x.methods) for x in rules}

    return jsonify(routes)
