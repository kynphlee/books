import json
import os
from pathlib import Path
from datetime import datetime

from munch import Munch, munchify  # type: ignore
import logging


logger = logging.getLogger('fake_service')


class ConfigDict(Munch):
    config_type = {'pythonhashseed': int}

    def load(self) -> None:
        runtime_params = self.get_runtime_params()
        config = self.get_config(runtime_params)
        config.update(runtime_params)
        self.create_logger(config)
        self.update(config)

    def get_runtime_params(self) -> Munch:
        rp = Munch()
        rp.start_time = datetime.utcnow()
        rp.project_path = Path(os.path.dirname(os.path.realpath(__file__))).parent
        rp.build_path = rp.project_path / 'build'
        rp.config_path = rp.build_path / 'config.json'
        rp.output_path = rp.project_path / 'output'

        return rp

    def get_config(self, runtime_params: Munch) -> Munch:
        with open(runtime_params.config_path, 'r') as f:
            config = munchify(json.load(f))

        self.overwrite_config(config)

        return config

    def overwrite_config(self, config: Munch) -> None:
        for k in config.keys():
            try:
                attr = os.environ[k.upper()]

                if attr:
                    try:
                        param_type = self.config_type[k]
                        config[k] = param_type(attr)
                    except KeyError:
                        config[k] = attr
            except KeyError:
                pass

    def create_logger(self, config: Munch):
        werkzeug_logger = logging.getLogger('werkzeug')
        werkzeug_logger.setLevel(logging.ERROR)

        log_path = config.output_path / 'log.txt'
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
        fh = logging.FileHandler(log_path)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    def to_dict(self):
        config_out = self.toDict()
        config_out['start_time'] = config_out['start_time'].strftime('%Y-%m-%d %H:%M:%S')
        config_out['project_path'] = str(config_out['project_path'])
        config_out['build_path'] = str(config_out['build_path'])
        config_out['config_path'] = str(config_out['config_path'])
        config_out['output_path'] = str(config_out['output_path'])

        return config_out


class Singleton(type):

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = ConfigDict(*args, **kwargs)
            cls._instance.load()
        return cls._instance


class Config(metaclass=Singleton):
    pass