from flask import jsonify, request

from fake_service.config import Config
from fake_service.version_0_1_0.main import app_v0_1_0
from fake_service.version_0_1_0.helpers import create_400
from fake_service.version_0_1_0.app import my_calculation


# Example endpoint and function returning json
# ----------------------------------------------------------------------------------------
@app_v0_1_0.route('/my_url/')
def my_endpoint():
    """
    Call: localhost:8080/v_0_1_0/my_url?x=10&y=20&z=30
    """
    x = int(request.args.get('x'))
    y = int(request.args.get('y'))
    z = int(request.args.get('z'))

    result = my_calculation(x, y, z)

    return jsonify(result)