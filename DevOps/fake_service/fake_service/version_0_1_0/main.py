from flask import Blueprint, render_template


# Main blueprint
# ----------------------------------------------------------------------------------------
version_value = '0_1_0'
version_name = 'version_' + version_value
version_url_name = 'v_' + version_value
folder_name = 'fake_service'
app_v0_1_0 = Blueprint(version_name, version_name,
                       static_folder=f'{folder_name}/{version_name}/static',
                       static_url_path='',
                       template_folder=f'{folder_name}/{version_name}/templates')


# Example endpoint and function returning html
# ----------------------------------------------------------------------------------------
@app_v0_1_0.route('/index/')
def index():
    """
    Call: localhost:8080/v_0_1_0/index
    """
    return render_template('index.html')
