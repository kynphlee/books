from flask import jsonify

# Helpers
# ----------------------------------------------------------------------------------------
def create_400(msg):
    data = {'status': 400, 'message': msg}
    response = jsonify(data)
    response.status_code = 400

    return response


def create_no_json_response():
    msg = 'No parameters provided. Add json body.'
    return create_400(msg)


def create_missing_fields_response(param_str):
    msg = f'Missing parameters: {param_str}'
    return create_400(msg)


def create_missing_config_fields_response(param_str):
    msg = f'Missing parameters in config file: {param_str}'
    return create_400(msg)