# fake_service

<br/>
Starter project created from dalsim service [Cookiecutter](https://git.delta.com/ods/dalsim/dalsim_service).

<br/>


## Background
The fake_service is a microservice that follows the cookie cutter template
for similar services used with the Delta Airlines simulator. The purpose of this document is
to introduce fake_service and to give more details instructions for
running this software. Please sdd a more detailed overview of your service here.

<br/>

## Quick Start
The build system can run the service from either a Docker container or a Python
virtual environment. For development or testing a virtual environment is the preferred 
method, while Docker is the preferred deployment mechanism for production.
Make is used for the build system with most of the build steps contained in Python and 
Bash scripts. An updated list of commands is shown at the bottom of this document.

By default running `make` without a target will run `make all` which itself is an
alias for `make run`. The `make all` and `make up` targets are simply an alias for `make run` and
they can be used interchangably. To run the service from a Docker container 
use the following shell command below:
```sh
$ make
```

or

```sh
$ make run
```

<br/>


## Virtual Environments
To run the service from a virtual environment no build step is required, but you must first setup the virtual environment.

To setup the virtual environment run:

```sh
make venv
``` 

Once the virtual environment is setup it needs to be activated by running the command:

```sh
source ./fake_service_env/bin/activate
```

Finally once the virtual environment is activated simply run the following make command to run the service:

```sh
make run-native
```

More information on virtual environments can be found [here](https://realpython.com/python-virtual-environments-a-primer/), and 
more information on Docker can be found [here](https://blog.codeship.com/why-docker/).

<br/>

## Environment Variables
There are many different variables that can precisely control how a build is done. For most
users, knowning only a few variables will be needed to run with the correct settings. To get a more
detailed understanding read the `Makefile` and the associated build scripts located in the folder: `./scripts/build`.

The short explanation is as follows.

Use the `config_vars` variable to set the parameters needed when runnning the application. If the 
`config_vars` variable is set to one of dev/prod/local the system will look for a similarly named
file under the directory `./config/env_vars` that has configuration options. 
If dev/prod/local is used the full path does not need to be spelled out and the file ending 
does not need to be specified. To use a file that is not one of those above then the 
`config_vars` can be set to the full path to the file with configuration options.

Docker example:
```sh
make run config_vars=locals
```

Alternative Docker example:
```sh
make run config_vars=./path_to_my_file/my_file.sh
```

Virtual environment example:
```sh
make run-native config_vars=dev
```

A longer explanation is that there are multiple other variables as well that can be set to control
the application at build time and at runtime. The application will load variables and run a different
way when it is in a Docker container than when the application is run natively without Docker. The 
json configuration variables can be specified, but as the application runs any configuration loaded
from the `config_vars` will overwrite the previous configuration setting. For most users simply
supplying the correct configuration options in a file and using the variable `config_vars` to specify
that file should be enough.


<br/>

## Build System Commands

| Command                   | Description                                                                   |
| ------------------------- | ----------------------------------------------------------------------------- |
| make                      | defaults to **make all**                                                      |
| make all                  | build and run service in Docker                                               |
| make test-venv            | verify running from within a virtual environment                              |
| make venv                 | create a virtual environment to run app from                                  |
| make config               | build config file                                                             |
| make requirements         | build requirements file                                                       |
| make security             | run Python security check                                                     |
| make build                | build Docker image                                                            |
| make clean                | remove build dir                                                              |
| make clean-artifacts      | remove all Docker dangling items                                              |
| make clean-all            | remove build dir, dandling Docker items, and Docker images                    |
| make run                  | build and run service in Docker                                               |
| make run-native           | build and run service from virtual environment in shell                       |
| make deploy               | deploy to Openshift                                                           |
| make stop                 | stop service running in Docker                                                |
| make up                   | build and run service in Docker                                               |
| make down                 | stop service running in Docker, remove build dir, and remove dangling items   |


<br/>

## Variables for Commands

| Command                   | Variables                                                                     |
| ------------------------- | ----------------------------------------------------------------------------- |
| make                      | tag, config_vars, venv_name, config_json, req_file, build_path                |
| make all                  | tag, config_vars, venv_name, config_json, req_file, build_path                |
| make test-venv            | venv_name                                                                     |
| make venv                 | venv_name                                                                     |
| make config               | config_json, build_path                                                       |
| make requirements         | req_file, build_path                                                          |
| make security             | venv_name                                                                     |
| make build                | venv_name, config_json, req_file, build_path                                  |
| make clean                | build_path                                                                    |
| make clean-artifacts      |                                                                               |
| make clean-all            | build_path                                                                    |
| make run                  | tag, config_vars, venv_name, config_json, req_file, build_path                |
| make run-native           | config_vars, test_venv, config_json, build_path                               |
| make deploy               | port                                                                          |
| make stop                 |                                                                               |
| make up                   | tag, config_vars, venv_name, config_json, req_file, build_path                |
| make down                 |                                                                               |


<br/>

## Build System Variables

| Variable    | Description                     |
| ----------- | ------------------------------- |
| config_json | Build configuation settings     |
| config_vars | Runtime configuration settings  |
| req_file    | Path the requirements file      |
| venv_name   | Virtualenv name                 |
| tag         | Docker tag                      |
| port        | Application port                |
| build_path  | Build path                      |