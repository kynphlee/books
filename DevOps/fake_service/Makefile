PROJECT_DIR=$(shell pwd)
SCRIPT_DIR=$(PROJECT_DIR)/scripts/build

ARTIFACT_REVISION=0.1.0
ARTIFACT_NAME=fake_service

# Help
# --------------------------------------------------------------------------------------------------
.PHONY: help

help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := all


# Override variables
# --------------------------------------------------------------------------------------------------
config_json=prod
config_vars=prod
req_file=prod
venv_name=fake_service_env
tag=latest
port=8080
build_path=$(PROJECT_DIR)/build


# Make commands
# --------------------------------------------------------------------------------------------------
all: run

test-venv:
	@python $(SCRIPT_DIR)/test_virtualenv.py -v $(venv_name)

venv: clean config requirements
	@python $(SCRIPT_DIR)/setup_virtualenv.py -v $(venv_name)

config:
	@python $(SCRIPT_DIR)/create_config.py -c $(config_json) -b $(build_path)

requirements:
	@python $(SCRIPT_DIR)/create_requirements.py -r $(req_file) -b $(build_path)

security: test-venv
	@python $(SCRIPT_DIR)/vuln_scan.py

build: security config requirements
	@python $(SCRIPT_DIR)/build.py -t $(tag)

build-in-docker:
	@python $(SCRIPT_DIR)/build_in_docker.py -b $(build_path)

clean:
	@python $(SCRIPT_DIR)/clean.py -b $(build_path)

clean-artifacts:
	@python $(SCRIPT_DIR)/clean_artifacts.py

clean-all: clean clean-artifacts
	@python $(SCRIPT_DIR)/clean_all.py

run: build
	@python $(SCRIPT_DIR)/run.py -t $(tag) -c $(config_vars)

run-in-docker:
	@python $(SCRIPT_DIR)/run_in_docker.py -p $(port)

run-native: security config
	@python $(SCRIPT_DIR)/run_native.py -c $(config_vars)

deploy:
	@python $(SCRIPT_DIR)/deploy.py -p $(port)

stop: 
	@python $(SCRIPT_DIR)/stop.py

up: run

down: stop clean-artifacts

.PHONY: all test-venv venv config requirements security build build-in-docker clean \
		clean-artifacts clean-all run run-in-docker run-native deploy stop up down

