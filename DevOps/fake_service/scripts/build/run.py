import sys
import os
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Run docker container.')
    parser.add_argument('-t', '--tag', type=str, help='name of docker tag')
    parser.add_argument('-c', '--config_vars', type=str, help='config(dev/prod/local or file_path)')

    return parser.parse_args()


def main():
    util.print_title('Run')
    tag = 'latest'
    args = parse_args()
    paths = util.get_project_paths()

    if args.tag:
        tag = args.tag

    if util.is_known_env(args.config_vars):
        ev_path = paths['project'] / 'config' / 'env_vars' / f'{args.config_vars}.sh'
    elif util.is_valid_file(args.config_vars):
        ev_path = args.config_vars
    else:
        util.print_red(f'ERROR -> config unknown type and is not file: {args.config_vars}')
        sys.exit(1)

    util.source_env_vars(str(ev_path))

    env = os.environ
    raw_cmd = (f'sudo docker run -it -h fake_service_app' 
               f' -e APP_NAME={env["APP_NAME"]}'
               f' -e APP_VERSION={env["APP_VERSION"]}'
               f' -e FLASK_ENV={env["FLASK_ENV"]}'
               f' -e FLASK_APP={env["FLASK_APP"]}'
               f' -e FLASK_PORT={env["FLASK_PORT"]}'
               f' -e PYTHONHASHSEED={env["PYTHONHASHSEED"]}'
               f' -p {env["FLASK_PORT"]}:{env["FLASK_PORT"]}'
               f' --network=host'
               f' --rm --name fake_service_app --dns 159.165.56.240 fake_service:{tag}')
    util.run_cmds(raw_cmd.split())

    sys.exit(0)


if __name__ == '__main__':
    main()