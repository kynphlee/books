import sys
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Copy config file to build directory.')
    parser.add_argument('-c', '--config_json', type=str, help='config(dev/prod/local or file_path)')
    parser.add_argument('-b', '--build_path', type=str, help='path to perform build')

    return parser.parse_args()


def main():
    util.print_title('Create Config')
    args = parse_args()

    if args.build_path:
        paths = util.get_project_paths(args.build_path)
    else:
        paths = util.get_project_paths()

    util.create_build_dir_if_missing(paths['build'])

    if util.is_known_env(args.config_json):
        src = paths['project'] / 'config' / f'{args.config_json}.json'
    elif util.is_valid_file(args.config_json):
        src = args.config_json
    else:
        util.print_red(f'ERROR -> config unknown type and is not file: {args.config_json}')
        sys.exit(1)

    dst = paths['build'] / 'config.json'
    util.copy_file(src, dst)

    sys.exit(0)


if __name__ == '__main__':
    main()
