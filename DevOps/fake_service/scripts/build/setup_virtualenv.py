from __future__ import print_function
import sys
import argparse


import util


def parse_args():
    parser = argparse.ArgumentParser(description='Setup a Python virtualenv for the project')
    parser.add_argument('-v', '--virtualenv', type=str, help='name of virtualenv')

    return parser.parse_args()


def main():
    # Script vars
    major = 3
    minor = 6
    v_env_name = 'fake_service_env'

    util.print_title('Create Virtualenv')
    args = parse_args()
    paths = util.get_project_paths()

    if args.virtualenv:
        v_env_name = args.virtualenv

    if not util.verify_python_ver(major, minor):
        sys.exit(1)

    if util.in_virtualenv():
        error_str = 'ERROR -> Virtualenv cannot be created while in another virtualenv\n'
        error_str += "Use command 'deactivate' to leave other virtualenv.\n"
        util.print_red(error_str)
        sys.exit(1)

    paths = util.get_project_paths(paths['build'])
    env_path = paths['project'] / v_env_name

    if env_path.exists():
        error_str = f'ERROR -> Virtualenv already exists at: {env_path}\n'
        error_str +='If you wish to recreate the virtualenv first remove the old one.\n'
        util.print_red(error_str)
        sys.exit(1)

    util.run_cmds(['virtualenv', v_env_name])

    pip_script = paths['script'] / 'pip_install.sh'
    cmds = f'{pip_script} -e dev -p {v_env_name}'.split()
    util.run_cmds(cmds)
    sys.exit(0)


if __name__ == '__main__':
    main()
