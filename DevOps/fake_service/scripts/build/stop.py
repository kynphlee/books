import sys

import util


def main():
    util.print_title('Stop')
    cmds = f'sudo docker stop fake_service_app'.split()
    util.run_cmds(cmds)
    sys.exit(0)


if __name__ == '__main__':
    main()
