import sys
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Build in docker container.')
    parser.add_argument('-b', '--build_path', type=str, help='path to perform build')

    return parser.parse_args()


def main():
    util.print_title('Build In Docker')
    args = parse_args()

    if args.build_path:
        paths = util.get_project_paths(args.build_path)
    else:
        paths = util.get_project_paths()

    cmds = f'pip install --upgrade pip'.split()
    util.run_cmds(cmds)
    cmds = f'pip install -r {paths["build"] / "requirements.txt"}'.split()
    util.run_cmds(cmds)

    sys.exit(0)


if __name__ == '__main__':
    main()