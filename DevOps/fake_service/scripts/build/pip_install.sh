#!/usr/bin/env bash


###############
# Script vars #
###############
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
project_dir=$(readlink -f "$script_dir"/../..)
build_dir="$project_dir"/build
. "$script_dir"/helpers.sh


#########
# Usage #
#########
function print_usage()
{
	printf "\e[1;34mThis script sources the env vars and runs the service.\e[0m\n\n"

	printf "\e[1;34musage: %s [-h] [-e env]\e[0m\n" "$program_name"
	printf "\e[1;34m  -h            help\e[0m\n"
    printf "\e[1;34m  -e env        specify environment(dev/prod)\e[0m\n"
    printf "\e[1;34m  -p path       specify path to virtualenv\e[0m\n"
}


#################
# Create config #
#################
function install_libs()
{
	if [ -z "$venv" ]; then
		printf "\e[31mERROR -> the virtualenv must be specified with -p\e[0m\n"
		exit 1
	fi

	if [ "$env" != 'dev' ] && [ "$env" != 'prod' ]; then
		printf "\e[1;31mERROR -> Env specified does not exist: %s\n\e[0m" "$env"
		exit 1
	fi

	venv_path="$project_dir"/"$venv"

	if [ ! -d "$venv_path" ]; then
		printf "\e[1;31mERROR -> Virtualenv specified does not exist: %s\n\e[0m" "$venv_path"
		exit 1
	fi

	printf "\e[1;34mUsing virtualenv: %s\n\e[0m" "$venv_path"
	source "$venv_path"/bin/activate

	printf "\e[1;34mUsing build env: %s\n\e[0m" "$env"
	pip install -r "$project_dir"/requirements/prod.txt

	if [ "$env" == 'dev' ]; then
		pip install -r "$project_dir"/requirements/dev.txt
	fi

	exit 0
}


###########
# Options #
###########
while getopts ":e:p:h" opt; do
	case $opt in
		e)
	  		env=$OPTARG
	  		;;
	  	p)
	  		venv=$OPTARG
	  		;;
	  	h)
			print_usage
			exit 1
			;;
		\?)
	  		printf "\e[1;34mInvalid option: -$OPTARG\e[0m\n" >&2
	  		exit 1
	  		;;
		:)
	  		printf "\e[1;34mOption -$OPTARG requires an argument.\e[0m\n" >&2
	  		exit 1
	  		;;
	esac
done


########
# Main #
########
install_libs