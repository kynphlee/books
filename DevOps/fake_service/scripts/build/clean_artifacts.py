import sys
import util


def main():
    util.print_title('Clean Artifacts')
    cmds = 'sudo docker image prune -f'.split()
    util.run_cmds(cmds)
    cmds = 'sudo docker system prune -f'.split()
    util.run_cmds(cmds)

    sys.exit(0)


if __name__ == '__main__':
    main()
