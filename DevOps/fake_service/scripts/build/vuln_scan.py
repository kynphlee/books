import sys

import util


def main():
    util.print_title('Vuln Scan')
    paths = util.get_project_paths()
    cmds = f'bandit {paths["project"] / "fake_service"} -r'.split()
    util.run_cmds(cmds, False)

    sys.exit(0)


if __name__ == '__main__':
    main()
