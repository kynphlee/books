import sys

import util


def main():
    util.print_title('Clean')
    cmds = 'sudo docker rmi -f fake_service'.split()
    util.run_cmds(cmds)

    sys.exit(0)


if __name__ == '__main__':
    main()