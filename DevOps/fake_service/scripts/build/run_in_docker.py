import sys
import os

import util


def main():
    util.print_title('Run In Docker')
    env = os.environ
    cmds = f'flask run -h 0.0.0.0 -p {env["FLASK_PORT"]}'.split()
    util.run_cmds(cmds)

    sys.exit(0)


if __name__ == '__main__':
    main()