import sys
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Build in docker container.')
    parser.add_argument('-t', '--tag', type=str, help='name of docker tag')

    return parser.parse_args()


def main():
    util.print_title('Build')

    tag = 'latest'
    args = parse_args()

    if args.tag:
        tag = args.tag

    cmds = 'sudo docker run --rm -i hadolint/hadolint'.split()
    util.run_cmds(cmds, False, 'Dockerfile')
    cmds = f'sudo docker build -t fake_service:{tag} --network host --rm .'.split()
    util.run_cmds(cmds)

    sys.exit(0)


if __name__ == '__main__':
    main()