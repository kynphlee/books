import sys
import shutil
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Clean up build path.')
    parser.add_argument('-b', '--build_path', type=str, help='path to perform build')

    return parser.parse_args()


def main():
    util.print_title('Clean')
    args = parse_args()

    if args.build_path:
        paths = util.get_project_paths(args.build_path)
    else:
        paths = util.get_project_paths()

    if paths['build'].exists():
        util.print_light_blue(f'Removing build dir: {paths["build"]}')
        shutil.rmtree(paths['build'])
    else:
        util.print_light_blue('Nothing to clean up. No build dir.')

    sys.exit(0)


if __name__ == '__main__':
    main()
