from __future__ import print_function
import sys
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Test Python and virtualenv are properly set.')
    parser.add_argument('-v', '--virtualenv', type=str, help='name of a valid virtualenv')

    return parser.parse_args()


def main():
    # Script vars
    major = 3
    minor = 6
    v_env_name = 'fake_service_env'

    util.print_title('Test Virtualenv')
    args = parse_args()

    if args.virtualenv:
        v_env_name = args.virtualenv

    if not util.verify_python_ver(major, minor):
        sys.exit(1)

    if not util.verify_virtual_env(v_env_name):
        sys.exit(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
