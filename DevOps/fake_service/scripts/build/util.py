import os
import sys
import shlex
from pathlib import Path
import subprocess
from shutil import copyfile


def print_title(title):
    try:
        rows, columns = os.popen('stty size', 'r').read().split()
    except Exception:
        columns = 100

    title_len = len(title)
    extra_length = int(columns) - title_len - 6
    side_len = int(extra_length / 2)

    side = ('-' * side_len)
    out = '{}|  {}  |{}'.format(side, title, side)
    print_light_blue(out)


def print_colored(text, color):
    print('{}{}\033[0m'.format(color, text))


def print_light_blue(text):
    print_colored(text, '\033[94m')


def print_red(text):
    print_colored(text, '\033[31m')


def get_project_paths(build_path=None):
    script_path = Path(os.path.dirname(os.path.realpath(__file__)))
    project_path = script_path.parent.parent
    output_path = project_path / 'output'

    if not build_path:
        build_path = project_path / 'build'
    else:
        build_path = Path(build_path)

    paths = {
        'script': script_path,
        'project': project_path,
        'build': build_path,
        'output': output_path}

    return paths


def is_known_env(env):
    envs = ['dev', 'prod', 'local']

    if env not in envs:
        return False

    return True


def is_valid_file(env):
    env = Path(env)

    if env.exists() and env.is_file():
        return True

    return False


def create_build_dir_if_missing(path):
    build_path = Path(path)

    if not build_path.exists():
        print_light_blue('Build path does not exist.')
        print_light_blue(f'Creating build path: {build_path}')
        build_path.mkdir()


def copy_file(src, dst):
    print_light_blue('Copying file')
    print_light_blue(f'\tFrom: {src}')
    print_light_blue(f'\tTo:   {dst}')
    copyfile(src, dst)


def append_file(src, dst, text):
    print_light_blue('Appending file')
    print_light_blue(f'\tFrom: {src}')
    print_light_blue(f'\tTo:   {dst}')

    with open(dst, 'a') as f:
        if text:
            f.write(text)

        f.write(open(src, 'r').read())


# ----------------------------------------------------------------------------------------------------------------------
# BASH
# ----------------------------------------------------------------------------------------------------------------------
def run_cmds(cmds, check=True, input=False):
    cmd_str = ' '.join(cmds)
    print_light_blue(f'Running > {cmd_str}')

    try:
        if input:
            with open(input, 'rb', 0) as f:
                subprocess.run(cmds, check=True, stdin=f)
        else:
            subprocess.run(cmds, check=True)
    except Exception:
        if check:
            print_red(f'ERROR -> command failed: {cmd_str}')
            sys.exit(1)


def source_env_vars(file_path):
    command = shlex.split(f"env -i bash -c 'source {file_path} && env'")
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)

    for line in proc.stdout:
        line = line.decode('utf-8')
        values = line.strip().split('=')
        os.environ[values[0]] = values[1]
    proc.communicate()


# ----------------------------------------------------------------------------------------------------------------------
# Virtual Environment
# ----------------------------------------------------------------------------------------------------------------------
def verify_python_ver(major, minor):
    ver = sys.version_info
    full_ver = ' '.join(sys.version.split('\n'))
    version_info = 'Using Python version: {}.{}\n'.format(major, minor)
    version_info += 'Details: {}\n'.format(full_ver)
    version_info += 'Full path to Python: {}\n'.format(sys.executable)

    if ver < (major, minor):
        error_str = 'ERROR -> Python version must be greater than: {}.{}\n'.format(major, minor)
        error_str += version_info

        print_red(error_str)

        return False

    print_light_blue(version_info)

    return True


def verify_virtual_env(name):
    if not in_virtualenv():
        error_str = 'ERROR -> Not running in a virtualenv\n'
        error_str += "To setup a virtualenv run 'make setup_venv'\n"
        error_str += "Once a virtualenv is setup it must be activated with 'source ./simulator_env/bin/activate'\n"
        error_str += "If you wish to use a virtualenv with a different path use the 'virtualenv' script parameter\n"

        print_red(error_str)

        return False

    actual_venv_name = get_virtualenv_name()

    if name != actual_venv_name:
        error_str = 'ERROR -> in incorrect virtualenv\n'
        error_str += f'Specified virtualenv name: {name}\n'
        error_str += f'Actual virtualenv name: {actual_venv_name}\n'
        error_str += "If you wish to use a virtualenv with a different path use the 'virtualenv' script parameter\n"

        print_red(error_str)

        return False

    return True


def in_virtualenv():
    return (hasattr(sys, 'real_prefix') or
            (hasattr(sys, 'base_prefix') and sys.base_prefix != sys.prefix))


def get_virtualenv_name():
    v_env = os.environ['VIRTUAL_ENV']

    return Path(v_env).name