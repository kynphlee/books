import sys
import time
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Deploy to openshift.')
    parser.add_argument('-p', '--port', type=str, help='port to expose')

    return parser.parse_args()


def main():
    util.print_title('Openshift Deploy')
    args = parse_args()
    paths = util.get_project_paths()

    cmd = f'oc login'.split()
    util.run_cmds(cmd)

    cmd = f'oc new-app {paths["project"]} --strategy=docker --source-secret=gitlab-delta'.split()
    util.run_cmds(cmd)

    time.sleep(5)

    cmd = f'expose dc/fake_service --target-port="{args.port}" --port={args.port}'.split()
    util.run_cmds(cmd)

    util.print_light_blue('Verify the following at https://<cluster>.ocp.delta.com:8443')
    util.print_light_blue('1) [Build] repo is https')
    util.print_light_blue('2) [Build] valid secret for source repo exists, using gitlab-delta by default')
    util.print_light_blue('3) [Route] there is a route defined')

    sys.exit(0)


if __name__ == '__main__':
    main()