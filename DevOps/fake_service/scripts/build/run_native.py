import sys
import argparse
import os

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Run native app.')
    parser.add_argument('-c', '--config_vars', type=str, help='environment(dev/prod/local)')

    return parser.parse_args()


def main():
    util.print_title('Run Native')
    args = parse_args()
    paths = util.get_project_paths()

    if util.is_known_env(args.config_vars):
        ev_path = paths['project'] / 'config' / 'env_vars' / f'{args.config_vars}.sh'
    elif util.is_valid_file(args.config_vars):
        ev_path = args.config_vars
    else:
        util.print_red(f'ERROR -> config unknown type and is not file: {args.config_vars}')
        sys.exit(1)

    if not paths['output'].exists():
        paths['output'].mkdir()

    util.source_env_vars(str(ev_path))

    env = os.environ
    cmds = f'flask run -h 0.0.0.0 -p {env["FLASK_PORT"]}'.split()
    util.run_cmds(cmds)
    sys.exit(0)


if __name__ == '__main__':
    main()