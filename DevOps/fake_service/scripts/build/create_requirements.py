import sys
import argparse

import util


def parse_args():
    parser = argparse.ArgumentParser(description='Combine requirements file into single file in build directory.')
    parser.add_argument('-r', '--req_file', type=str, help='libs(dev/prod or file_path)')
    parser.add_argument('-b', '--build_path', type=str, help='path to perform build')

    return parser.parse_args()


def main():
    util.print_title('Create Requirements')
    args = parse_args()

    if args.build_path:
        paths = util.get_project_paths(args.build_path)
    else:
        paths = util.get_project_paths()

    util.create_build_dir_if_missing(paths['build'])

    if util.is_known_env(args.req_file):
        if args.req_file == 'dev':
            src = paths['project'] / 'requirements' / 'prod.txt'
            dst = paths['build'] / 'requirements.txt'
            util.copy_file(src, dst)

            src = paths['project'] / 'requirements' / 'dev.txt'
            header = '\n\n# Dev packages\n'
            header += f'# {"-" * 15}\n'
            util.append_file(src, dst, header)
        else:
            src = paths['project'] / 'requirements' / f'{args.req_file}.txt'
            dst = paths['build'] / 'requirements.txt'
            util.copy_file(src, dst)
    elif util.is_valid_file(args.req_file):
        src = args.req_file
        dst = paths['build'] / 'requirements.txt'
        util.copy_file(src, dst)
    else:
        util.print_red(f'ERROR -> requirements unknown type and is not file: {args.req_file}')
        sys.exit(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
