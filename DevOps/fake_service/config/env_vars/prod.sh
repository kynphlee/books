#!/usr/bin/env bash

export APP_NAME=fake_service
export APP_SHORT_NAME=faker
export APP_VERSION=0.1.0
export FLASK_ENV=production
export FLASK_APP=./fake_service/app.py
export FLASK_PORT=8080
export PYTHONHASHSEED=0
